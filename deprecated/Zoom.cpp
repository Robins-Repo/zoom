/*
* This code is Copyright (c) Daniel (Robin) Smith
* Author: Daniel (Robin( Smith <smithtrombone@gmail.com> 2020 - Present
*
* See README for more information about this project.
* See LICENSE for more information on use and distribution of this code.
*/

//Zoom expects the terminal to be mostly ECMA-48 compliant

////////////////////////////////////////////////////////////////////////////////
//	INCLUDES
////////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <stdio_ext.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <pwd.h>
#include <time.h>
#include <string>
#include <dirent.h>
#include <exception>
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	GLOBAL DECLARATIONS AND CONSTANTS
////////////////////////////////////////////////////////////////////////////////
//	STRING CONSTANTS========================================================
const char
//contains version number
*VERSION = {"0.1.0 Pre-Alpha"},
//prints command line options for Zoom
*HELP {"Zoom [?vif] (forms)\n?\tPrints this message.\nv\tPrints the \
version number.\ni\tPrints instructions for the Zoom interface.\nf\tAllows an \
arbitrary number of Lisp forms to be loaded into the REPL on initialization.\n\
forms\tThe Lisp forms being added after option\"f\"\n"},
//prints the Zoom interface key bindings to the command line
*BINDINGS {"The Zoom Interface\n\tZoom has two vertically stacked windows \
that present a Lisp REPL on top of a text entry area. The user interacts with \
the bottom window to enter Lisp forms into the REPL. The output from the REPL \
will be drawn in the top window.\n\tWhen Zoom starts, the user can type on the \
keyboard to enter text into the bottom window. The user can access various \
additional functions by combining letters with the Ctrl key. Zoom has the \
following key bindings:\n\
Ctrl + q\tExits Zoom.\n\
Arrow Keys\tMoves the cursor around the window.\n\
Ctrl + i\tToggles the text input mode between Overtype (default) and Insert.\n\
Ctrl + c\tSends the input window to the REPL, but doesn't clear the window.\n\
Ctrl + d\tSends the input window to the REPL, and clears the window.\n\
Ctrl + w\tToggles hiding the text editor to use Zoom as a traditional REPL shell\n\
Ctrl + l\tScrolls the output window downward.\n\
Ctrl + k\tScrolls the output window upward.\n\
Ctrl + z\tIterates through previous input forms.\n\
Ctrl + x\tIterates through autocomplete suggestions.\n\
Ctrl + s\tExports the current text in the input window to a text file.\n\
Ctrl + e\tLoads a text file into the input window buffer.\n\
Ctrl + o\tImports a text file into the input window.\n\
Ctrl + a\tToggles colors on and off\n\
CTRL + r\tRefreshes the screen\n"};
//======KEYCODE CONSTANTS=======================================================
#define CTRL_Q 17
#define CTRL_W 23
#define CTRL_S 19
#define CTRL_A 1
#define CTRL_D 4
#define CTRL_O 15
#define CTRL_C 3
#define CTRL_E 5
#define CTRL_N 14
#define CTRL_L 12
#define CTRL_K 11
#define CTRL_Z 26
#define CTRL_X 24
#define CTRL_P 16
#define CTRL_R 18
#define CUR_UP -1
#define CUR_DOWN -2
#define CUR_LEFT -3
#define CUR_RIGHT -4
//======ANSI ESCAPE CODES=======================================================
#define NEXT_LINE "\r\n"
#define CLEAR_LINE "\x1b[K"
#define CLEAR_SCREEN "\x1b[2J"
#define SETC_I "\x1b[?25l"
#define SETC_V "\x1b[?25h"
#define SETC_TOPL "\x1b[1;1H"
#define UNDERLINE "\x1b[4m"
#define NO_STYLE "\x1b[0m"
#define BOLD "\x1b[1m"
//======TAB LENGTH==============================================================
#define TABL 8
//======TERMINFO PATHS==========================================================
const char
*TERMINFO_DIR[2] = {"/usr/share/terminfo/", "/usr/lib/terminfo/"};
//======TRUECOLOR TERMINAL ESCAPE CODES=========================================
//Here's a list of Solarized colors to choose from for true color terminals
//base03: 0 43 54		| dark blue
//base02: 7 54 66		| navy blue
//base01: 88 110 117		| dark gray that matches 03 and 02
//base00: 101 123 131		| light gray that matches 03 and 02
//base0: 131 148 150		| light gray that matches 3 and 2
//base1: 147 161 161		| dark gray that matches 3 and 2
//base2: 238 232 213		| tan
//base3: 253 246 227		| egshell white
//yellow: 181 137 0
//orange: 203 75 22
//red 220 50 47
//magenta: 211 54 130
//violet: 108 113 196
//blue: 38 139 210
//cyan 42 161 152
//green 133 153 0
//======BASE EDITOR AND DISPLAY COLORS==========================================
//background for output window
#define BG1 "\x1b[48;2;0;43;54m"
//background for input window
#define BG2 "\x1b[48;2;238;232;213m"
//foreground for output window
#define FG1 "\x1b[38;2;147;161;161m"
//foreground for input window
#define FG2 "\x1b[38;2;131;148;150m"
//background for cursor
#define BGC "\x1b[48;2;253;246;227m"
//color for line numbers
#define LN "\x1b[38;2;42;161;m"
//======SYNTAX HIGHLIGHTING COLORS==============================================
//parentheses, "'", "[]", and "|"
#define TERMINATING "\x1b[38;2;133;153;0m"
//"\"
#define ESCAPE "\x1b[38;2;203;75;22;m"
#define STRINGS ""
#define SYMBOLS
/*
HL1 = "\x1b[38;2;211;54;138m",
//highlight 2 is for symbols and quotes
*HL2 = "\x1b[38;2;203;75;22m",
//highlight 3 is for function names
*HL3 = "\x1b[38;2;42;161;152m",
//highlight 4 is for parentheses
*HL4 = "\x1b[38;2;133;153;0m";
*/
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	THE ZOOM VIRTUAL MACHINE CLASS
////////////////////////////////////////////////////////////////////////////////

class __ZVM
{
public:
private:
	struct addr
	{
		char c;
		addr *a, *b;
	};
	long long int reg [16];
	long double freg[8];
	addr *p;
};



////////////////////////////////////////////////////////////////////////////////
//	THE REPL CLASS
////////////////////////////////////////////////////////////////////////////////

//this class runs the REPL and manages the memory for it.

class __REPL
{
public:
	__REPL ();
	~__REPL ();
	void RUN(std::vector<std::string>&output, std::string input);
//initialized the REPL, or resets it if called after the class has been initiated
	std::string init (std::vector <std::string> initForms = std::vector<std::string> (1, ""));
//function to pass matches back to autocomplete
	std::string MATCH (std::string pattern);
	void operator = (__REPL _);
//flags operations that trace back to the editor
//0 is no flags;
//1 is the (ed) command was called
	int flag;
private:
//string for the editor to edit
	std::string ed;
/*
	This string is the standard readtable.
	The positions of the characters correspond to the ascii table.
	Characters can be re-mapped to different roles within the table while the representation of each role is preserved
*/
	const char* STANDARD_READTABLE = "\0\0\0\0\0\0\0\a\b\t\n\v\f\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 !:#$%&;()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[//]^_`abcdefghijklmnopqrstuvwxyz{|}~\0";
//readtable case flag: 0 = :upcase, 1 = :downcase, 2 = :preserve, 3 = :invert
	int READTABLE_CASE = 0;
	std::vector <std::string> RTABLES;
	std::string CRDT;
//readtable cases: :upcase = 0, :downcase = 1, :preserve = 2, :invert = 3
	int readtableCase = 0;
/*
	language hierarchy
	1: Environment Object: global, "nil", user defined
	2: Packages: "COMMON-LISP", "COMMON-LISP-USER", "KEYWORD", Readtables: current, "standard", initial
	3: Symbols: 'foo, 'bar, etc...
	4: System Classes: T, integer, function
*/
//structures to create the lisp environment
	struct ENV;
	struct package;
	struct symbol;
	struct object;
	struct property;
//type specifier
	struct type
	{
//name of type
		std::string name;
//list of supertypes for named type
		std::vector<std::string>supertypes;
	};
//basic data object; can have a type and some data
	struct object
	{
//type structure to name type
		type t;
//data stored as a string of characters to be interpreted during evaluation
		std::string data;
	};
//property struct for symbol properties
	struct property
	{
//two objects, indicator is the searchable property indicator and value
		object indicator, value;
	};
//the basic operatable type in common lisp
	struct symbol
	{
//symbol name
		std::string name;
//package symbol resides in
		package *p;
//value is where the symbol holds data as a variable
		object *value;
//specifies whether symbol is a function or not
		bool function;
//list of symbol properties
		std::vector<property>properties;
//to address scope, symbol can be declared shadowed
		bool shadowed;
		int t = 1;
	};
//holds declared symbols
	struct package
	{
//name of package
		std::string name;
//nicknames of package
		std::vector<std::string> nicknames;
//interned symbols go here
		std::vector<symbol*> internal;
//externed symbols go here
		std::vector<symbol*> external;
	};
	struct ENV
	{
		std::vector<package>p;
	};
	ENV GLOBAL_ENV;
	struct atom
	{
		atom *p;
		void *a, *b;
		int t = 0;
	};
//performs the Reader algorithm
//atom is the root of the S-expression tree from the perspective of the read function body
//s is the string being read
//pos is the position marking the character being read from the string
//parens is true if the read algorithm has read a left parenthesis
	void read (atom *a, std::string &s, int &pos, int &parens);
	symbol eval (atom *a);
	bool constChar (char c);
//for the "optimize command"
	int compilation_speed = 0, debug = 0, safety = 0, space = 0, speed = 0;
};
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	THE EDITOR CLASS
////////////////////////////////////////////////////////////////////////////////

//The __EDITOR class uses only ECM-48 ASCII sequences

class __EDITOR
{
public:
	__EDITOR ();
	~__EDITOR ();
//The EDITOR is externally invoked through this function
	void START (std::vector<std::string> forms);
private:
//this allows the editor to wrap the REPL
	__REPL REPL;
//contains previous state of terminal before Zoom was invoked
	termios preserve;
//contains the terminal state of the editor
	termios editor;
//this is a vector of strings corresponding to the number of rows in the
//terminal screen
	std::vector<std::string> OBUFFER, IBUFFER;
//holds all the previous input strings
	std::vector<std::vector<std::string>>previousInput;
//how far the previous input has been scrolled back
	int previousInScroll = 0;
//holds input in shell mode
	std::string shellInput = " ";
//holds past shell inputs;
	std::vector<std::string>prevShellInput;
	int prevShellScroll = 0;
//true if the gui hasn't looped once yet
	bool isInit = true;
//tells if the terminal window has been resized
	bool resized = false;
//tells if Overtype mode is active
	bool overtype = true;
//tells if Zoom is in shell or editor mode
	bool shell = true;
//tells if the UI is colored or not
	bool colored = false;
//represents the cursor position in the input string
//shellC is the cursor position in shell mode
	int cx = 0, cy = 0, shellC = 0;
//represents height of the input windows
	int inputH;
//represents the height of the output window
	int outputH;
//window information struct from termios.h
	winsize WIN;
//offsets
//ISC, OSC, offsets for vertical scrolling
//HISC, HOSC, offsets for horizontal scrolling
//IT, OT, offsets to keep track of rendered size with tabs
	int ISC = 0, OSC = 0, HISC = 0, HOSC = 0;
//======__EDITOR INTERNAL FUNCTIONS=============================================
//gets window size
	void getWinSize ();
//prints a line in the input at row "s"
	void setScreen (int s);
//set screen overload for printing entire screen
	void setScreen ();
//gets key input
	bool handleKey ();
//copies input text to REPL
	void softPush ();
//passes input text to REPL, clearing input buffer
	void hardPush ();
//resets the terminal and exits the program
	void restore ();
//quits in case of error
	void hardExit (const char *s);
//displays cursor
	void drawCur ();
//formats syntax highlighting
	void syntax (std::string &in);
};
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	MAIN (ENTRY POINT)
////////////////////////////////////////////////////////////////////////////////
int main (int argN, char **argC)
{
//make some useful variables
	std::string TERM;
	int *termDir, *termFile;
	char mode = '\0';
	int ret;
	FILE *f;
	termios backup;
//put all the forms in here if argument "f" was provided
	std::vector <std::string> forms;
//multi-purpose catchall string
	std::string tmp;
//initialize REPL and Editor
	__EDITOR e;
//get terminal name
	TERM = getenv ("TERM");
	if (TERM.size () == 0)
		perror ("Couldn't get name of terminal!\nPlease set the TERM en\
vironment variable with the name of your terminal.");
	else
	{
//get terminfo directory
		if (!opendir(TERMINFO_DIR[0]))
			if (!opendir (TERMINFO_DIR[1]))
				perror ("Couldn't find terminfo database!");
			else
			{
				tmp.clear ();
				tmp.append (TERMINFO_DIR[1]);
				tmp.push_back (TERM[0]);
				tmp.push_back ('/');
				tmp.append (TERM);
				if ((f = fopen (tmp.c_str(), "r")) == 0)
					perror ("Couldn't open terminfo file!");
				else
				{

				}
			}
		else
		{

		}
	}
//determine arguments provided
	if (argN > 1)
	{
//get first argument
			mode = argC[1][0];
//determine which one
		switch (mode)
		{
			case 'v':
			{
				std::cout << VERSION << std::endl;
				return 0;
			}
			case '?':
			{
				std::cout << HELP << std::endl;
				return 0;
			}
			case 'i':
			{
				std::cout << BINDINGS << std::endl;
				return 0;
			}
			case 'f':
			{
				for (int i = 0; i < argN - 2; i ++)
				{
					forms.push_back (std::string (argC[i]));
				}
			}
			case '\0':
			{
				break;
			}
			default:
			{
				std::cout << HELP << std::endl;
				return 0;
			}
		}
	}
	tcgetattr (STDIN_FILENO, &backup);
//======pass off execution to main editor loop==================================
	try
	{
		e.START (forms);
	}
	catch (std::exception &t)
	{
		write (STDOUT_FILENO, "\x1b[0m", 4);
	        write (STDOUT_FILENO, "\x1b[2J", 4);
	        write (STDOUT_FILENO, "\x1b[H", 3);
	        write (STDOUT_FILENO, SETC_V, 6);
		tcsetattr (STDOUT_FILENO, TCSAFLUSH, &backup);
		std::cout << "There was an unexpected error!\n";
		std::cout << t.what () << std::endl;
	}
	return 0;
}
/******************************************************************************/




////////////////////////////////////////////////////////////////////////////////
//	EDITOR MEMBER FUNCTION DEFINITIONS
////////////////////////////////////////////////////////////////////////////////
__EDITOR::__EDITOR ()
{

}
__EDITOR::~__EDITOR ()
{

}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
void __EDITOR::START (std::vector<std::string> forms)
{
//save terminal state===========================================================
	if (tcgetattr (STDIN_FILENO, &preserve) == -1)
		hardExit ("tcgetattr failed");
//change terminal settings with second term struct==============================
	editor = preserve;
        editor.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	editor.c_oflag &= ~OCRNL;
	editor.c_oflag |= OPOST;
        editor.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
        editor.c_cflag |= CS8;
        editor.c_cc[VMIN] = 0;
        editor.c_cc[VTIME] = 0;
//apply "raw mode"==============================================================
	if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &editor) == -1)
		hardExit ("tcsetattr failed!");
//pass formd to the REPL========================================================
	REPL.init (forms);
//turn off terminal cursor======================================================
	write (STDOUT_FILENO, SETC_I, sizeof (SETC_I));
//start main interface loop=====================================================
	const struct timespec waitPeriod [] {0, 1000000L};
	while (1)
	{
		getWinSize ();
		if (resized)
		{
			setScreen ();
		}
		handleKey ();
		nanosleep (waitPeriod, NULL);
	}
}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
void __EDITOR::getWinSize ()
{
//======save old window size for comparison=====================================
	int oldw = WIN.ws_col, oldh = WIN.ws_row;
//======get window size with ioctl==============================================
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &WIN) == -1 or WIN.ws_col == 0)
		hardExit ("Couldn't get window size!");
	else
	{
//======compare old window size to new size=====================================
		if (oldw != WIN.ws_col or oldh != WIN.ws_row)
		{
//======if window size changed, adjust sub windows==============================
			resized = true;
			outputH = WIN.ws_row / 4;
			inputH = WIN.ws_row - outputH;
			//if (WIN.ws_row % 2 == 0)
				//inputH = (WIN.ws_row / 3) * 2;
			//else
			//	inputH = 1 + (WIN.ws_row / 2) * 3;
		}
//======if windows size is the same, do nothing=================================
		else
			resized = false;
	}
}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
void __EDITOR::setScreen ()
{
//======create local temporary variables========================================
	int o = 0, nl = 0;
	std::string tmp, check;
//======if window is too small print a warning==================================
	if (WIN.ws_col < 32 or WIN.ws_row < 4)
	{
		hardExit ("Screen is too small!\nMinimum screen size is 32 characters across and 4 characters high.");
	}
//======initialization stuff when zoom is first opened==========================
	if (isInit)
	{
		OBUFFER.resize (4);
		OBUFFER.at(0).assign (";Welcome to Zoom!");
		OBUFFER.at(1).assign (";Copyright (c) 2020");
		OBUFFER.at(2).assign (";Daniel (Robin) Smith");
		OBUFFER.at(3).assign (";Zoom conforms with the requirements of ANSI ");
		IBUFFER.resize (1);
		//cy = 0;
		//cx = 0;
		isInit = false;
	}
//======check if zoom is in shell mode==========================================
	if (shell)
	{
		tmp.clear ();
		tmp.append (NO_STYLE);
		tmp.append (SETC_TOPL);
		if (colored)
		{
			tmp.append (BG1);
			tmp.append (FG1);
		}
		for (int i = OSC; i < WIN.ws_row + OSC - o - 1; i ++)
		{
			tmp.append (CLEAR_LINE);
			if (i < OBUFFER.size ())
			{
				tmp.append (OBUFFER.at(i));
				tmp.append ("\n");
				if ((OBUFFER.at(i).size () / WIN.ws_col > 0))
				{
					o += OBUFFER.at(i).size () / WIN.ws_col;
					if (OBUFFER.at(i).size () % WIN.ws_col > 0)
						o ++;
				}
			}
			else
			{
				if (i < WIN.ws_row + OSC - o - 1)
					tmp.append ("\n");
			}
		}
		for (int i = 0; i < WIN.ws_col; i ++)
			tmp.append ("-");
		//tmp.append ("-");
		if (colored)
		{
			tmp.append (BG2);
			tmp.append (FG2);
		}
		tmp.append (CLEAR_LINE);
		if (shellInput.size () > 0)
		{
			if (shellC < shellInput.size ())
			{
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (">");
				tmp.append (shellInput.substr (0, shellC));
				if (colored)
					tmp.append (LN);
				tmp.append (UNDERLINE);
				tmp.push_back (shellInput.at (shellC));
				tmp.append (NO_STYLE);
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (shellInput.substr (shellC + 1, std::string::npos));
			}
			else
			{
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (">");
				tmp.append (shellInput);
				if (colored)
					tmp.append (LN);
				tmp.append (UNDERLINE);
				tmp.append (" ");
				tmp.append (NO_STYLE);
				if (colored)
				{
					tmp.append (BG1);
					tmp.append (FG1);
				}
			}
		}
		else
		{
			if (colored)
			{
				tmp.append (BG2);
				tmp.append (FG2);
			}
			tmp.append (">");
			if (colored)
				tmp.append (LN);
			tmp.append (UNDERLINE);
			tmp.append (" ");
			tmp.append (NO_STYLE);
			if (colored)
			{
				tmp.append (BG1);
				tmp.append (FG1);
			}
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
	}
	else
	{
//======print the output window text============================================
		tmp.clear ();
		tmp.append (NO_STYLE);
		tmp.append (SETC_TOPL);
		if (colored)
		{
			tmp.append (BG1);
			tmp.append (FG1);
		}
		for (int i = OSC; i < outputH + OSC - o; i ++)
		{
			tmp.append (CLEAR_LINE);
			if (i < OBUFFER.size ())
			{
				tmp.append (OBUFFER.at(i));
				tmp.append ("\n");
				if ((OBUFFER.at(i).size () / WIN.ws_col) > 0)
					o += OBUFFER.at(i).size () / WIN.ws_col;
			}
			else
			{
				if (i < outputH + OSC - o)
					tmp.append ("\n");
			}
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
//======print the input window==================================================
//======make some variables for accounting for tab expansions===================
//off keeps track of how many characters the tabs expand the string to
		int off, len, i;
//isY is true if the current line being processed has the cursor in it
		bool isY;
//======clear output buffer string==============================================

		tmp.clear ();
		tmp.append ("\x1b[");
		tmp.append (std::to_string (outputH + 1));
		tmp.append (";1H");
		if (colored)
		{
			tmp.append (BG2);
			tmp.append (FG2);
		}
		for (i = 0; i < inputH - 1; i ++)
		{
			tmp.append (CLEAR_LINE);
			tmp.append ("\n");
		}
		tmp.append ("\x1b[");
		tmp.append (std::to_string (outputH + 1));
		tmp.append (";1H");
		len = 0;
		i = ISC;
//======loop through lines of input=============================================
		for (int h = ISC; h < inputH + ISC - len; h ++)
		{
			tmp.append (CLEAR_LINE);
//======determine if this line has the cursor===================================
			if (cy == i)
				isY = true;
			else
				isY = false;
//======if there is a line at this height, process it to the buffer=============
			if (i < IBUFFER.size ())
			{
//======add line marker=========================================================
				off = 0;
				for (int j = 0; j < IBUFFER.at(i).size (); j ++)
				{
					if (IBUFFER.at(i).at(j) == '\t')
						off += ((j + off) % TABL);
				}
//======subtract number of lines left in loop for every line over 1 that this
//======line has
				len += (IBUFFER.at(i).size () + off + 3) / WIN.ws_col;
				if (isY)
				{
					if (cx < IBUFFER.at(i).size ())
					{
						tmp.append (IBUFFER.at(i).substr (0, cx));
						if (colored)
						{
							tmp.append (BGC);
							tmp.append (LN);
						}
						tmp.append (UNDERLINE);
						tmp.push_back (IBUFFER.at(i).at(cx));
						tmp.append (NO_STYLE);
						if (colored)
						{
							tmp.append (BG2);
							tmp.append (FG2);
						}
						tmp.append (IBUFFER.at(i).substr (cx + 1, std::string::npos));
					}
					else
					{
						tmp.append (IBUFFER.at(i));
						if (colored)
						{
							tmp.append (BGC);
							tmp.append (LN);
						}
						tmp.append (UNDERLINE);
						tmp.append (" ");
						tmp.append (NO_STYLE);
						if (colored)
						{
							tmp.append (BG2);
							tmp.append (FG2);
						}
					}
				}
				else
				{
					tmp.append (IBUFFER.at(i));
				}
				if (i < inputH + ISC - len - 1)
					tmp.append ("\n");
			}
			else
				if (i < inputH + ISC - len - 1)
					tmp.append ("\n");
			i++;
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
	}
}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
bool __EDITOR::handleKey ()
{
	bool isKey = false;
	char key[7] = {0};
	if (read (STDIN_FILENO, key, 7) == -1 and errno != EAGAIN)
		hardExit ("read error!");
	if (key[0] == 0)
	{
		return false;
	}
//======variables for all cases defined here to avoid cross initialization======
	char a[1];
	std::string save, tmp;
	bool err = false;
	std::ifstream in;
//======if an escape sequence was entered=======================================
	if (key[0] == 27)
	{
		switch (key[2])
		{
//======Up arrow key============================================================
			case 'A':
			{
				if (shell)
				{
					if (prevShellInput.size () > 0)
					{
						if (prevShellScroll < prevShellInput.size () - 1)
							prevShellScroll ++;
						shellInput.assign (prevShellInput.at(prevShellScroll));
					}
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy - ISC == 0)
					{
						if (ISC > 0)
							ISC --;
						if (cy > 0)
							cy --;
					}
					else
						if (cy > 0)
							cy --;
					if (cx > IBUFFER.at(cy).size ())
					{
						cx = IBUFFER.at(cy).size ();
						if (cx > WIN.ws_col - 1)
						{
							HISC = cx - WIN.ws_col - 1;
						}
					}
				}
				setScreen ();
				break;
			}
//======Down arrow key==========================================================
			case 'B':
			{
				if (shell)
				{
					if (prevShellScroll > 0)
					{
						prevShellScroll --;
						if (prevShellScroll == 0)
						{
							shellInput.clear ();
							shellC = 0;
						}
						else
							shellInput.assign (prevShellInput.at(prevShellScroll));
					}
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy - ISC == inputH - 1)
					{
						if (cy - ISC > 0)
							ISC ++;
						if (cy < IBUFFER.size () - 1)
							cy ++;
					}
					else
					{
						if (cy < IBUFFER.size () - 1)
							cy ++;
					}
					if (cx > IBUFFER.at(cy).size ())
					{
						cx = IBUFFER.at(cy).size ();
						if (cx > WIN.ws_col - 1)
						{
							HISC = cx - WIN.ws_col - 1;
						}
					}
				}
				setScreen ();
				break;
			}
//======Right arrow key=========================================================
			case 'C':
			{
				if (shell)
				{
					if (shellC < shellInput.size ())
						shellC ++;
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy < IBUFFER.size ())
					{
						if (cx < IBUFFER.at(cy).size ())
						{
							cx ++;
							if (cx > WIN.ws_col - 1)
								HISC ++;
						}
					}
				}
				setScreen ();
				break;
			}
//======Left arrow key==========================================================
			case 'D':
			{
				if (shell)
				{
					if (shellC > 0)
						shellC --;
				}
				else
				{
					if (cy < IBUFFER.size ())
					{
						if (cx > 0)
						{
							if (HISC > 0)
								HISC --;
							cx --;
						}
					}
				}
				setScreen ();
				break;
			}
		}
		return true;
	}
	switch (key[0])
	{
//======quits zoom==============================================================
		case CTRL_Q:
		{
			restore ();
			break;
		}
//======hard push to the REPL, clearing the input window========================
		case CTRL_D:
		{
			if (shell)
				break;
			std::string tmp;
			for (int i = 0; i < IBUFFER.size (); i++)
			{
				OBUFFER.push_back (IBUFFER.at(i));
				tmp.append (IBUFFER.at(i));
				tmp.append ("\n");
			}
			REPL.RUN (OBUFFER, tmp);
			previousInput.insert (previousInput.begin (), IBUFFER);
			IBUFFER.clear ();
			cx = 0;
			cy = 0;
			ISC = 0;
			if (OBUFFER.size () > outputH)
			{
				OSC = OBUFFER.size () - outputH;
			}
			setScreen ();
			break;
		}
//======toggles entry modes between overtype and insert
		case CTRL_O:
		{
			overtype = !overtype;
			break;
		}
//======soft push to the REPL, doesn't clear the input window===================
		case CTRL_C:
		{
			if (shell)
				break;
			std::string tmp;
			for (int i = 0; i < IBUFFER.size (); i ++)
			{
				OBUFFER.push_back (IBUFFER.at(i));
				tmp.append (IBUFFER.at(i));
				tmp.append ("\n");
			}
			REPL.RUN (OBUFFER, tmp);
			if (OBUFFER.size () > outputH)
			{
				OSC = OBUFFER.size () - outputH;
			}
			setScreen ();
			break;
		}
		case CTRL_N:
		{
			break;
		}
		case CTRL_W:
		{
			if (OBUFFER.size () > WIN.ws_row)
			{
				OSC = OBUFFER.size () - WIN.ws_row;
			}
			shell = !shell;
			setScreen ();
			break;
		}
		case CTRL_A:
		{
			colored = !colored;
			setScreen ();
			break;
		}
		case CTRL_K:
		{
			if (OSC < OBUFFER.size () - 1)
			{
				OSC ++;
				setScreen ();
			}
			break;
		}
		case CTRL_L:
		{
			if (OSC > 0)
			{
				OSC --;
				setScreen ();
			}
			break;
		}
		case CTRL_Z:
		{
			if (previousInput.size () == 0)
				break;
			if (previousInScroll >= previousInput.size ())
				break;
			else
			{
				if (previousInScroll == 0)
				{
					previousInput.insert (previousInput.begin (), IBUFFER);
					previousInScroll ++;
				}
				IBUFFER = previousInput.at(previousInScroll);
				if (previousInScroll < previousInput.size ())
					previousInScroll ++;
				cx = 0;
				cy = 0;
				ISC = 0;
				HISC = 0;
				setScreen ();
			}
			break;
		}
		case CTRL_X:
		{
			if (previousInScroll > 0)
			{
				previousInScroll --;
				IBUFFER = previousInput.at(previousInScroll);
				cx = 0;
				cy = 0;
				ISC = 0;
				HISC = 0;
				setScreen ();
			}
			break;
		}
		case CTRL_E:
		{
			previousInput.push_back (IBUFFER);
			tmp.append (NO_STYLE);
			tmp.append (SETC_TOPL);
			tmp.append (CLEAR_SCREEN);
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			tmp.assign(SETC_TOPL);
			tmp.append ("Please enter the path/name of the lisp form you want to load.\n");
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			save.clear ();
			while (1)
			{
				a[0] = 0;
				if (read (STDIN_FILENO, a, 1) == -1 and errno != EAGAIN)
					hardExit ("read error!");
				if (a[0] == 0)
				{
					const struct timespec waitPeriod [] {0, 1000000L};
					nanosleep (waitPeriod, NULL);
					continue;
				}
				if (a[0] == 13 or a[0] == 10)
					break;
				if (a[0] == 127 or a[0] == 10)
				{
					write (STDOUT_FILENO, "\r", 1);
					write (STDOUT_FILENO, CLEAR_LINE, sizeof (CLEAR_LINE));
					save.pop_back ();
					write (STDOUT_FILENO, save.c_str (), save.size ());
				}
				if (a[0] < 127 and a[0] > 31)
				{
					save.push_back (a[0]);
					write (STDOUT_FILENO, a, 1);
				}
			}
			FILE *in;
			in = fopen (save.c_str (), "r");
			if (in == 0)
			{
				OBUFFER.push_back ("Z) Error opening lisp form!");
				OBUFFER.push_back (save);
			}
			else
			{
				OBUFFER.push_back ("Z) Loading lisp form.");
				OBUFFER.push_back (save);
				IBUFFER.clear ();
				IBUFFER.emplace_back ();
				while (fread (a, 1, 1, in) != 0)
				{
					if (a[0] > 31)
					{
						IBUFFER.back ().push_back (a[0]);
					}
					else
					{
						IBUFFER.emplace_back ();
					}
				}
			}
			cx = 0;
			cy = 0;
			HISC = 0;
			ISC = 0;
			setScreen ();
			break;
		}
		case CTRL_S:
		{
			tmp.append (NO_STYLE);
			tmp.append (SETC_TOPL);
			tmp.append (CLEAR_SCREEN);
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			tmp.assign(SETC_TOPL);
			tmp.append ("Please type the path/name where you want to save your lisp form\n");
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			save.clear ();
			while (1)
			{
				a[0] = 0;
				if (read (STDIN_FILENO, a, 1) == -1 and errno != EAGAIN)
					hardExit ("read error!");
				if (a[0] == 0)
				{
					const struct timespec waitPeriod [] {0, 1000000L};
					nanosleep (waitPeriod, NULL);
					continue;
				}
				if (a[0] == 13 or a[0] == 10)
					break;
				if (a[0] == 127 or a[0] == 10)
				{
					write (STDOUT_FILENO, "\r", 1);
					write (STDOUT_FILENO, CLEAR_LINE, sizeof (CLEAR_LINE));
					save.pop_back ();
					write (STDOUT_FILENO, save.c_str (), save.size ());
				}
				if (a[0] < 127 and a[0] > 31)
				{
					save.push_back (a[0]);
					write (STDOUT_FILENO, a, 1);
				}
			}
			FILE *out;
			out = fopen (save.c_str (), "w");
			if (out == 0)
			{
				OBUFFER.push_back ("Error saving lisp form to file!");
				OBUFFER.push_back (save);
			}
			else
			{
				OBUFFER.push_back ("Z) Saving Lisp Form to file:");
				OBUFFER.push_back (save);
				for (int i = 0; i < IBUFFER.size (); i ++)
				{
					if (fwrite (IBUFFER.at(i).c_str (), 1, IBUFFER.at(i).size (), out) == 0)
					{
						OBUFFER.push_back ("Z) Error saving Lisp Form to file!");
						OBUFFER.push_back ("Line ");
						OBUFFER.back ().append (std::to_string (i));
						err = true;
					}
					if (fwrite ("\n", 1, 1, out) == 0)
					{
						OBUFFER.push_back ("Z) Error saving Lisp Form to file!");
						OBUFFER.push_back ("Line ");
						OBUFFER.back ().append (std::to_string (i));
						err = true;
					}
				}
				if (err)
					OBUFFER.push_back ("Z) Lisp Form saved with errors!");
				else
					OBUFFER.push_back ("Z) Lisp Form saved without errors.");
			}
			setScreen ();
			break;
		}
		case CTRL_R:
		{
			getWinSize ();
			setScreen();
			break;
		}
//======Case for new line and carriage return characters========================
		case 13:
		case 10:
		{
			if (shell)
			{
				if (shellInput.size () > 0)
					prevShellInput.insert (prevShellInput.begin (), shellInput.c_str ());
				prevShellScroll = 0;
				OBUFFER.push_back (shellInput);
				REPL.RUN (OBUFFER, shellInput);
				shellInput.clear ();
				shellC = 0;
				if (OBUFFER.size () > WIN.ws_row)
				{
					OSC = OBUFFER.size () - WIN.ws_row;
				}

			}
			else
			{
				previousInScroll = 0;
				if (IBUFFER.size () == 0)
				{
					IBUFFER.resize (2);
					cy = 1;
					cx = 0;
				}
				else
				{
					if (cy + 1 <= IBUFFER.size ())
					{
						if (cx >= IBUFFER.at(cy).size ())
							IBUFFER.emplace (IBUFFER.begin () + cy + 1);
						else
						{
							IBUFFER.insert (IBUFFER.begin () + cy + 1, IBUFFER.at(cy).substr (cx, std::string::npos));
							IBUFFER.at(cy).erase (cx, std::string::npos);
						}
						cy ++;
						cx = 0;
					}
					else
					{
						if (cx >= IBUFFER.at(cy).size ())
							IBUFFER.emplace_back ();
						else
						{
							IBUFFER.push_back (IBUFFER.at(cy).substr (cx, std::string::npos));
							IBUFFER.at(cy).erase (cx, std::string::npos);
						}
						cy ++;
						cx = 0;
					}
				}
				if (cy >= inputH)
					ISC ++;
			}
			setScreen ();
			break;
		}
//======case for ascii delete "backspace" key===================================
		case 8:
		case 127:
		{
			if (shell)
			{
				if (shellInput.size () == 0)
					break;
				if (shellC > 0)
				{
					shellInput.erase (shellC - 1, 1);
					shellC --;
				}
			}
			else
			{
				previousInScroll = 0;
				if (IBUFFER.size () == 0)
					break;
				if (cx + HISC > 0)
				{
					IBUFFER.at(cy).erase (cx - 1, 1);
					if (cx > 0)
						cx --;
					else
					{
						HISC --;
					}
				}
				else
				{
					if (cy != 0)
					{
						IBUFFER.erase (IBUFFER.begin () + cy);
						cy --;
						cx = IBUFFER.at(cy).size ();
					}
				}
			}
			setScreen ();
			break;
		}
		default:
		{
			if (shell)
			{
				if (key[0] < 127 and key[0] > 31)
				{
					prevShellScroll = 0;
					if (shellInput.size () == 0)
					{
						shellInput.clear ();
						shellInput.assign (&key[0]);
						shellC ++;
					}
					else if (shellC == shellInput.size ())
					{
						shellInput.push_back (key[0]);
						shellC ++;
					}
					else
					{
						if (overtype)
						{
							shellInput.at(shellC) = key[0];
							shellC ++;
						}
						else
						{
							shellInput.insert (shellC, &key[0]);
							shellC ++;
						}
					}
				}

			}
			else
			{
				if ((key[0] < 127 and key[0] > 31) or key[0] == 9)
				{
					previousInScroll = 0;
					if (IBUFFER.size () == 0)
					{
						IBUFFER.push_back (&key[0]);
						cx ++;
					}
					else
					{
						if (cx == IBUFFER.at(cy).size ())
						{
							IBUFFER.at(cy).insert (cx, &key[0]);
							cx ++;
						}

						else
						{
							if (overtype)
							{
								IBUFFER.at(cy).at(cx) = key[0];
								cx ++;
							}
							else
							{
								IBUFFER.at(cy).insert (cx, &key[0]);
								cx ++;
							}
						}
					}
				}
			}
			setScreen();
			break;
		}
	}
	return true;
}
//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
void __EDITOR::restore ()
{
        if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &preserve) == -1)
                hardExit ("restore failed!");
        write (STDOUT_FILENO, "\x1b[0m", 4);
        write (STDOUT_FILENO, "\x1b[2J", 4);
        write (STDOUT_FILENO, "\x1b[H", 3);
        write (STDOUT_FILENO, SETC_V, 6);
        std::cout << "Thank you for using Zoom!\n";
        exit(0);
}
//attempts to remove Zooms influence on terminal settings in case of error
void __EDITOR::hardExit (const char *s)
{
        tcsetattr (STDIN_FILENO, TCSAFLUSH, &preserve);
        write (STDOUT_FILENO, "\x1b[0m", 4);
//clear the screen
        write(STDOUT_FILENO, "\x1b[2J", 4);
//move cursor to left
        write(STDOUT_FILENO, "\x1b[H", 3);
        write (STDOUT_FILENO, SETC_V, 6);
//print error and exit program
        perror (s);
        exit (-1);
}
//displays cursor
void __EDITOR::drawCur ()
{
//this function is deprecated
}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	REPL MEMBER FUNCTION DEFINITIONS
////////////////////////////////////////////////////////////////////////////////
__REPL::__REPL ()
{

}
__REPL::~__REPL ()
{

}
std::string __REPL::init (std::vector <std::string> initForms)
{
	if (initForms.size () == 0)
		return "";
	else
		return "Success!";
	RTABLES.resize (0);
	RTABLES.push_back (std::string(STANDARD_READTABLE));
	CRDT.assign (RTABLES.front ());
}
void __REPL::RUN (std::vector<std::string>&output, std::string input)
{
	atom start;
	int pos = 0, parens = 0;
	read (&start, input, pos, parens);
	eval (&start);
}
std::string __REPL::MATCH (std::string pattern)
{
	return std::string ("suggestion");
}
void __REPL::read (atom *a, std::string &s, int &pos, int &parens)
{
//number of escape characters
	int e;
	std::string token;
	while (pos > s.size())
	{
//whitespace
//left-parentheses
//right-parentheses
//single quote
//semicolon
//double quote
//backquote
//comma
//sharpsign
//single escape (backslash)
//multiple escape (pipe)
	}
}
bool __REPL::constChar (char c)
{
	if (
		c == CRDT.at(8) or
		c == CRDT.at(9) or
		c == CRDT.at(10) or
		c == CRDT.at(11) or
		c == CRDT.at(12) or
		c == CRDT.at(13) or
		c == CRDT.at(32) or
		c == CRDT.at(40) or
		c == CRDT.at(41) or
		c == CRDT.at(39) or
		c == CRDT.at(59) or
		c == CRDT.at(34) or
		c == CRDT.at(96) or
		c == CRDT.at(44) or
		c == CRDT.at(35) or
		c == CRDT.at(92) or
		c == CRDT.at(124)
	)
		return false;
	else
		return true;
}
__REPL::symbol __REPL::eval (atom *a)
{
	symbol r;
	return r;
}
/******************************************************************************/
